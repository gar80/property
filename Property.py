from datetime import datetime
import numpy as np
import mysql.connector


class UKProperty(object):
    # A simple class to model a UK Property

    def __init__(self, user_inputs):
        # Initializes the attributes so the variables can be called later on
        self.price = user_inputs['price']
        self.area = user_inputs['area']
        self.taxband = user_inputs['taxband']
        self.bedrooms = user_inputs['bedrooms']
        self.bathrooms = user_inputs['bathrooms']
        self.square_meters = user_inputs['square_meters']
        self.mortgage_length = user_inputs['mortgage_length']
        self.mortgage_rate = user_inputs['mortgage_rate']
        self.mortgage_down = user_inputs['mortgage_down']


    def property_details(self):
        # Return the property details
        print("------------------------------------------------------------------")
        print("The property you are looking at costs: £ " + str(self.price) + " "
        + "and is located in: " + self.area + ".\nThe tax band is : " + self.taxband
        + ".\nThe property has:  "  + str(self.bedrooms) + "  bedrooms" + " and is  "  +
        str(self.square_meters) + "  square meters in size" + " and has " +
        str(self.bathrooms) + " bathrooms.")

    def property_average_price(self):
        # Mocked average property prices for 3 london locations
        areas = {
            'tooting':  {0: 129000, 1: 350000, 2: 450000, 3: 650000, 4: 800000, 5: 950000},
            'balham' : {0: 123000, 1: 400000, 2: 500000, 3: 750000, 4: 850000, 5: 1000000},
            'clapham' : {0: 290000, 1: 480000, 2: 682000, 3: 892000, 4: 975000, 5: 1000000}
        }
        price_difference = 0
        average_price = 0
        area_average = areas[self.area]
        average_price = area_average[int(self.bedrooms)]
        self.average_price_for_area = average_price
        if int(average_price) > int(self.price):
            price_difference = int(average_price) - int(self.price)
            print("This property is less than the average price by £ " + str(price_difference), ".")
        elif int(self.price) > int(average_price):
            price_difference = int(self.price) - int(average_price)
            print("This property is more than the average price by £ " + str(price_difference), ".")

    def price_per_sq_meter(self):
        # Calculates price per square meter based on user input
        print("The price per SQAURE METER is £ ", float(self.price) / float(self.square_meters), ".")
        self.price_per_square_meter = float(self.price) / float(self.square_meters)

    def price_per_bedroom(self):
        # Calculates price per bedroom based on user input
        if int(self.bedrooms) >= 1:
            print("The price per bedroom is  £", int(self.price) // int(self.bedrooms), ".")

    def mortgage(self):
        # Standard mortgage calculation to return monthly mortgage payments based on user input
        Down_Payment = float(self.mortgage_down)
        Property_Price = float(self.price)
        Mortgage_Amount = Property_Price*(1-Down_Payment/100)
        Mortgage_Type = float(self.mortgage_length)
        Mortgage_Term = int(12*Mortgage_Type)
        Interest_Rate = float(self.mortgage_rate)
        R = 1 +(Interest_Rate)/(12*100)
        X = Mortgage_Amount*(R**Mortgage_Term)*(1-R)/(1-R**Mortgage_Term)
        Mortgage_payments = str(np.round(X,2))
        self.Mortgage_payments = Mortgage_payments
        print("*****  You are putting " , self.mortgage_down, "% as a downpayment.  ***** ")
        return self.Mortgage_payments

    def DB_input(self, isflat):
        # Write user input to database
        mydb = mysql.connector.connect(
          host="localhost",
          user=(User_val),
          password=(Password_val),
          database="property_db"
        )

        mycursor = mydb.cursor()

        if isflat == "yes":
            sql = "INSERT INTO properties (Price, Area, Bedrooms, Bathrooms, Sqaure_meters, Tax_band, Average_price_for_area, Price_per_square_meter, Mortgage, Lease, Service_fee, Lease_extension_year, Service_Fee_year, Down_payment_percentage) VALUES (%s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s)"
            val = (self.price, self.area, self.bedrooms, self.bathrooms, self.square_meters, self.taxband, self.average_price_for_area, self.price_per_square_meter, self.Mortgage_payments, self.lease, self.service, self.lease_ext_year, self.service_fee_year_cost, self.mortgage_down)
        else:
            sql = "INSERT INTO properties (Price, Area, Bedrooms, Bathrooms, Sqaure_meters, Tax_band, Average_price_for_area, Price_per_square_meter, Mortgage, Down_payment_percentage) VALUES (%s, %s, %s, %s, %s, %s, %s , %s, %s, %s)"
            val = (self.price, self.area, self.bedrooms, self.bathrooms, self.square_meters, self.taxband, self.average_price_for_area, self.price_per_square_meter, self.Mortgage_payments, self.mortgage_down)

        mycursor.execute(sql, val)
        mydb.commit()
        print(mycursor.rowcount, "record inserted.")

class Flat(UKProperty):
    # Extended functionality of UKProperty to accomodate flats/apartments

    def __init__(self, user_inputs, lease, service):
        # Initializes the attributes unique to flats/apartments
        super(Flat, self).__init__(user_inputs)
        self.lease = lease
        self.service = service

    def flat_details(self):
        # Returns flat/apartment details based on user input
        print("-----------------------------------------------------------------")
        print("So you went for a flat then...ok the lease is: ", self.lease, "years, and the service fee is £ " ,self.service, "per month.")

    def service_fee_year(self):
        # Returns service fee details based on user input
        self.service_fee_year_cost = int(self.service) * 12
        print("Your service fee over a year will be £", int(self.service) * 12, ".")
        print("Your service fee over 25 years will be £", int(self.service) * 12 * 25, ".")

    def years_until_lease_extension(self):
        # Returns lease extension information based on user input
        currentyear = datetime.now().year
        lease_ext_year = currentyear + int(self.lease) - 82
        self.lease_ext_year = lease_ext_year
        print("-----------------------------------------------------------------")
        print("When the lease gets to 82 years you will need to start the process of extending the lease.")
        print("Based on your lease length you will need to extend the lease in ", int(self.lease) - 82, " years.")
        print("You will need to extend your lease in the year: ", currentyear + int(self.lease) - 82, ".")
        print("-----------------------------------------------------------------")


def userInput():
    # Builds user_input values based on user input
    user_input = {'price': "", "bedrooms": "", "bathrooms": "", "area": "", "taxband": "", "square_meters": "", "mortgage_length": "", "mortgage_rate": ""}
    approved_areas = ["tooting", "balham", "clapham"]
    tax_bands = ["A", "B", "C"]
    testdata = input("Do you wish to enter test data? ")
    if testdata == "yes":
        print("Ok, we will use the predefined test data for this appication run.")
        user_input = {'price': 85000, 'area': 'tooting', 'taxband': 'C', 'bedrooms': 0, 'bathrooms': 1, 'isflat': 'yes', 'square_meters': 50, 'mortgage_length': 25, 'mortgage_rate': 4, 'mortgage_down': 10 }
        return user_input
    else:
        while not user_input['price']:
            price = input("Enter a price, for example 500000: ")
            if price.isdigit():
                user_input['price'] = price
            else:
                print("Error: you must enter a number for the price!")

        while not user_input['bedrooms']:
            bedrooms = input("Enter the number of bedrooms: ")
            if bedrooms.isdigit() and int(bedrooms) <= 5:
                user_input['bedrooms'] = bedrooms
            else:
                print("Error: you must enter a number between 1 and 5 for the bedrooms!")

        while not user_input['bathrooms']:
            bathrooms = input("Enter the number of bathrooms: ")
            if bathrooms.isdigit():
                user_input['bathrooms'] = bathrooms
            else:
                print("Error: you must enter a number for the bathrooms!")

        while not user_input['area']:
            area = input("Enter an area, must be either Tooting, Balham, or Clapham: ")
            area = area.lower()
            if area in approved_areas:
                user_input['area'] = area
            else:
                print(user_input['area'], "is not valid. Area must be either Tooting, Balham, or Clapham!")

        while not user_input['taxband']:
            taxband = input("Enter a tax band A, B, or C:  ")
            taxband = taxband.upper()
            if taxband in tax_bands:
                user_input['taxband'] = taxband
            else:
                print("Error: Taxband must be either A, B, Or C!")

        while not user_input['square_meters']:
            square_meters = input("How many square meters is the property? ")
            if square_meters.isdigit():
                user_input['square_meters'] = square_meters
            else:
                print("Error: Square meters must be a number!")

        while not user_input['mortgage_length']:
            mortgage_length = input('Enter mortgage length in years, e.g 15 for 15 years: ')
            if mortgage_length.isdigit():
                user_input['mortgage_length'] = mortgage_length
            else:
                print("Error: Mortgage length must be a number!")

        while not user_input['mortgage_rate']:
            try:
                interest = float(input('Enter your Mortgage interest rate as a percentage, e.g 4 for 4%: '))
                if interest <= 100:
                    user_input['mortgage_rate'] = interest
                else:
                    print("Error: Mortgage rate must be a number between 1 and 100!")

            except ValueError:
                print("Error: Mortgage rate must be a number!")

        user_input['mortgage_down'] = input("Enter downpayment as a percentage for example 5 for 5% or 0 if you are putting nothing down: ")
        user_input['isflat'] = input("Is the property a flat yes or no? ")

        print("_______________________________________________________________________")
        print("_______________________________________________________________________")
        return user_input

# The main code
prop_details = userInput()

if prop_details['isflat'] == "yes":
    lease_length = input("What is the lease length? ")
    service_fee = input("What is the monthly service fee? ")
    myflat = Flat(prop_details, lease_length, service_fee)
    myflat.property_details()
    myflat.price_per_sq_meter()
    myflat.price_per_bedroom()
    myflat.property_average_price()
    myflat.flat_details()
    myflat.service_fee_year()
    myflat.years_until_lease_extension()
    print("------------------------------------------------------------")
    mortage_cost = float(myflat.mortgage())
    total_ownership = mortage_cost + float(myflat.service)
    print("Your mortgage will cost £", mortage_cost, " per month.")
    print("Your mortgage and monthly ownership costs are £", total_ownership, ".")
    myflat.DB_input(prop_details['isflat'])
else:
    myhouse = UKProperty(prop_details)
    myhouse.property_details()
    myhouse.price_per_sq_meter()
    myhouse.price_per_bedroom()
    myhouse.property_average_price()
    print("Your mortgage will cost £", myhouse.mortgage(), " per month.")
    myhouse.DB_input(prop_details['isflat'])
